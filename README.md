<meta charset="utf-8">
# Innovation 2000

jean-morgane@79.87.130.164:git/innovation-2000.git

Je met à disposition notre répertoire de travail (git) concernant la rédaction d'un texte critique sur l'innovation dans le design.
Pour l'instant il contient principalement des notes, prises pour et durant l'échange ESISART/ESAD et les deux jours Promising au magasin à grenoble.

J’espère commenter ces notes ou en faire une sorte de témoignage argumenté dans les prochains jours.

J’espère également que cela pourra vous servir d'une manière ou d'une autre ou nous permettre d'écrire ensemble.

framapad : https://annuel.framapad.org/p/innovation_2000
fiche_projet ByteBeatCoin : http://79.87.130.164/innovation-2000/www/fiche_projet.html
Notes intervention esisart esad : http://79.87.130.164/innovation-2000/www/innovation.intervention_esisart_esad.html
Notes innovation et image + promising : http://79.87.130.164/innovation-2000/www/innovation_et_image.promising.html
Index du dossier : http://79.87.130.164/innovation-2000/www/
Dossier en ligne : http://79.87.130.164/innovation-2000/ (j'éteinds le server la nuit)
Gitlab  : https://gitlab.com/design-non-garanti/innovation-2000
