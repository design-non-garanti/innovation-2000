# Bytebeatcoin

__Supervisors__: Cunin Dominique, Greis Émile, Maxime Bouton

__Emails__: <dominique.cunin@esad-gv.fr>, <emile.greis@esad-gv.fr>, <maxime.bouton@esad-gv.fr>

__Keywords__: Retrocomputing – Low-tech – Low-fi – Disnovation – Cryptocurrency – Music – Sound – Waveform – Generative – Portable device – Low-def screen

__Skills:__ a lot of lol

## Contexte du projet

Innovation as a rhetorical tool, innovation as social acceptance.

After a few thoughts on the history of *innovation* as term and politic principle toward its tendency to be a permanent demand, we find interesting to work on a retro computing object as an innovative project.

With this object we focus on the embed ambiguity of the *innovation* principle, in particular its relationship with obsolescence and reactivation, re-actualization and capitalism.

The idea behind this object is simple, since music doesn't generate enough money, why not make money generate music?

A proposal between irony and popularization of self-sustaining universal income access.

## Project Description and deliverables:

The idea is to build a portable device that generate bytebeat music through
cryptominning.

Initiated by Ville-Matias Heikkilä in 2011, bytebeat can be defined as "short
computer programs, sometimes consisting of as few as three arithmetic operations
in an infinite loop [that] can generate data that sounds like music when output
as raw PCM audio". These short programs follow the form:

    main ()
    {
        int t=0;
        for(;;t++) putchar(EXPRESSION);
    }

Using the total amount of resolved hashes during cryptomining as the iterative
variable of a bytebeat formula will generate sound based on computing power.

## Resources

- *L'innovation qu'est ce que c'est ?*, http://www.cite-sciences.fr/archives/francais/ala_cite/expositions/observatoire-innovations/definition-innovation/definition-innovation-1.html
- Anthony Masure, *Le design des programmes* (Inauthenticités de l'innovation), 2014, http://www.softphd.com/these/walter-benjamin-authenticites/inauthenticites-innovation
- Benoit Godin, *The Politics of Innovation : Machiavelli and Political Innovation, or, How to Stabilize a Changing World*, http://www.csiic.ca/PDF/WorkingPaper17.pdf
- Machiavel, *Le Prince*, 1513
- Nicolas Maigret & Bertrand Grimault , *Statement*, disnovation.org/curation.php
- Kragen, *Bytebeat*, http://canonical.org/~kragen/bytebeat/, 2011
- Ville-Matias Heikkilä, *Algorithmic symphonies from one line of code -- how and why?*,http://viznut.fi/texts-en/bytebeat_algorithmic_symphonies.html, 2011
- Ville-Matias Heikkilä, *Some deep analysis of one-line music programs*, http://viznut.fi/texts-en/bytebeat_deep_analysis.html, 2011
- Ville-Matias Heikkilä, *Discovering novel computer music techniques by exploring the space of short computer programs*, http://viznut.fi/texts-en/bytebeat_exploring_space.pdf, 2011






