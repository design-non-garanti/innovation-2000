<meta charset="utf-8">

# Innovation et image.
> Innovation, un non sens ?

Innovation : une rhétoriqe de la domination
Innovation : une rhétorique de la raison
Innovation : un discours d'auto-conservation
Innovation et mythe de la raison
Une forme de discours de la raison moderniste/universelle.
Une forme de discours des _grands/vertureux_ (voir Machiavel / Eco ?)

voir _Le concept de Lumières_ sur Wikipédia de la dialectique de la raison.

> https://fr.wikipedia.org/wiki/Dialectique_de_la_Raison

## Histoire et innovation 

<!--
Rappeler l'ancienneté de l'histoire du principe d'innovation n'est-ce pas déjà formuler une objection à l'hégémonie de son usage contemporain ?

Décrire le principe d'innovation comme une longue histoire

Décrire l'innovation comme un principe historique, est-ce une manière de faire apparaître les contradictions de son usage contemporain ?
-->

La page anglophone de la notion d'innovation sur wikipédia ne contient pas de partie historique tandis qu'il s'agit du point d'entrée de la version francophone.

Rappeler que le principe d'innovation a toujours existé en révèle les contradictions.

- L'Innovation est un imaginaire

> Innovation is everywhere. In the world of goods (technology) certainly, but also in the realm of words. Innovation is discussed in scientific and technical literature, in social sciences such as sociology, management and economics, and in the humanities and arts. Innovation is also a central idea in the popular imaginary, in the media and in public policy. How has innovation acquired such a central place in our society? 

> Innovation: The History of a Category, Benoît Godin, 2008, p3.

> We have a “violent Fondness for change, and greater Eagerness after Novelties”.

_Nous avons un penchant violent pour le changement et un amour brutal pour la nouveauté._

> B. Mandeville, The Fable of the Bees, 1732 in Innovation: The History of a Category, Benoît Godin, 2008, p4.

- Innovation et ubiquité ? La nature numérique de l'innovation contemporaine, innovation et technologie.

Le principe d'innovation est spontanément associé à l'innovation technologique.
Il s'agit peut-être de différentes formes de discours d'un même pouvoir/projet ?

Voir Olia Lialina pour ordinateur et technologie.

Le terme de numérique comme substitut à la notion d'ordinateur ubiquitaire.

Ordinateur : une innovation de la culture du livre (universalité, lumières).

innovation, technologie et capital (monde des marché, finance) = ordinateur ?

> IHC, p5.

> every individual is to a certain extent innovative; artists are innovative, scientists are innovative, and so are organizations in their day-to-day operations. 

> IHC, p6.

_Designer as Activist as Bullshitter as Barista_ de Silvio Lorusso

> Imitation → Invention → Innovation

> IHC, p8.

Innovation et même/ et image.

Perspective/Renaissance et innovation.

> IHC, p10.

## L'innovation a besoin d'un recyclage

La page anglophone apparaît achevée.

> Si ce bandeau n'est plus pertinent, retirez-le. Cliquez pour voir d'autres modèles. Cet article contient un travail inédit ou des déclarations non vérifiées (février 2013).
> Vous pouvez aider en ajoutant des références ou en supprimant le contenu inédit. Voir la page de discussion pour plus de détails.
> 
> Si ce bandeau n'est plus pertinent, retirez-le. Cliquez pour voir d'autres modèles. Cet article n’est pas rédigé dans un style encyclopédique (février 2013).
> Vous pouvez améliorer sa rédaction !
> 
> Sur la page discussion de la page francophone on trouve :

La page francophone apparaît comme contenant un travail inédit ou des déclarations non vérifiées, non rédigé dans un style encyclopédique.

> Innovation / Invention
>
> Il me semble que toute cette page parle de l'application de l'innovation, mais pas de sa nature technique, qui est la transposition d'une idée/expérience qui marche dans un domaine (y compris celui de l'invention) dans un ou plusieurs autre(s) domaine(s), y compris celui de la réalité ?
> 
> Ceci me parait avoir des conséquences importantes au niveau de la compréhension de l'usage stratégique de l'innovation, car la politique d'une dominance technique est de maintenir une tension vers l'innovation sur un marché dont elle veut contrôler l'évolution, dans la crainte d'une rupture due à une invention nouvelle qui la supplante.
> 
> C'est par exemple ce à quoi nous assistons dans le cas de l'Internet où les "stakeholders" de la dominance actuelle utilisent la place architecturale et le flou de la langue anglaise pour maintenir un climat d'innovation permanente supposé suffire au monde car suffisant au marché américain. Et bloquer depuis plusieurs années l'apparition de l'Internet Multilingue (principalement demandé et proposé par l'Asie [Chine] et permis par les contributions AFNOR et l'analyse française) et Sémantique (où la force particulière de la langue française et/ou de l'effort IA japonais qu'ils ont craint au départ devrait changer la parité technologique et affecter la stratégie de puissance des USA qui ont misé beaucoup sur la dominance mondiale par les technologies de l'information, alors qu'ils ne disposent d'une technologie Internet très ancienne). C'est le slogan connu de Vint Cerf "on ne répare pas ce qui marche" et on continue d'innover. Même si ce qui marche ne satisfait pas une très large part du marché qui a besoin d'invention et plus seulement d'innovation.
> 
> En ce sens l'Internet actuel est un cas d'école de l'utilisation de l'aura/légende de l'innovation (d'ailleurs quasi absente) pour se prémunir d'un risque de renversement du marché par l'invention.— Le message qui précède, non signé, a été déposé par 82.241.91.24 (discuter), le 28 septembre 2007 à 18:51 (HAE). 

---

> Recyclage de l'article
> 
> Je pense aussi que le recyclage de cet article est nécessaire, mais qu'il y a tout à faire.
>
> Les deux critiques de base que je permets de formuler sont les suivantes :
> 
> Manque d'une distinction formelle, claire entre, d'une part, le processus managérial, la démarche, la fonction, l'esprit d'innovation : LA innovation et, d'autre part, le résultat concret de ce processus, UNE ou des innovations.
> 
> L'absence quasi totale d'exemples, concrets, récents, actuels soit d'entreprises « innovantes », soit d'innovations génériques (tablette tactile, phablette, réseau social, boisson énergisante, etc.) ou spécifiques (iPad, Samsung Galaxy Note, Facebook, Red Bull, A380, etc.) avec les liens correspondants aux excellents articles de Wikipedia.
> 
> Qui d'entre vous accepterait de m'aider dans cette démarche en me donnant sa petite liste de, ce qui pour lui, sont des innovations remarquables récentes qui change la vie du monde ou remplacent l'existant bien mise en valeur et bien documenté par Wikipédia et ceci dans tous les domaines de l'innovation : technologique, social, informatique, etc.?
> 
> L'analyse du plus grand dénominateur commun de vos listes devrait me permettre de vous proposez une définition concrète de ce qu'est UNE innovation.
> 
> Merci d'avance de vos conseils et de votre aide. --Jean-Louis Swiners (d) 7 février 2013 à 20:55 (CET)
> 
> Je suis potentiellement intéressée car je suis tombée sur [2] et j'ai eu le sentiment que ton recyclage était tout de même un peu brutal. J'avoue aussi ne pas être fana du méga coup de promotion sur divers consultants via la section "définition". Pas du tout fan... Je vais donc attendre un peu et éventuellement modifier derrière toi. Je trouve également dommage le sabrage massif sur les "techniques". Un spécialiste comme toi trouve peut être cela inintéressant, mais le lecteur moyen y trouve son compte. Donc je suggère soit la remise en place, soit la séparation en un article détaillé sur le sujet. Anthere (d) 9 février 2013 à 13:19 (CET)
> 
> Anthere, merci pour ton aide.
> Je suis d'accord avec toi, mon recyclage a été un peu brutal. C'était pour faire avancer un recyclage qui n'avancait pas beaucoup.
> Je suis d'accord avec toi pour le coup de promotion de divers consultants via la section définition. J'avais trouvé les choses en l'état pour deux d'entre eux. Je supprimais ou je continuais. Je ne me suis pas permis de supprimer. J'ai continué. Tu as eu raison de supprimer mais il va falloir trouver une solution pour présenter les différentes définitions
> Le sabrage des techniques. Il s'agissait d'un inventaire d'outils, de techniques, etc. de management qui ne sont pas spécifiques à l'innovation. Je suis pour la séparation en un article détaillé (comme pour un article détaillé sur les techniques de créativité commune à la créativité et à la résolution créative de problème).
> J'aurais besoin d'une sous-catégorie du genre « Liste d'innovations » pour lister les innovations marquantes ou importantes de notre temps (l'idée m'a été donnée au départ par l'inclusion de l'article Jerrican dans Innovation). Qu'en penses-tu ? Je ne sais pas le faire. Peux-tu le faire ?
> Merci d'avance --Jean-Louis Swiners (d) 10 février 2013 à 12:16 (CET)
> 
> Oui, après lecture approfondie sur les outils, j'ai admis que ctn n'avaient rien à voir avec la choucroute. J'ai ajouté pour le moment un lien vers l'article sur la créativité. Ce n'est pas idéal mais mieux que rien.
> Faire une sous catégorie, c'est possible oui. Mais le risque que j'y vois est que classer un produit ou un service dans une catégorie catégorie:liste d'innovation risque d'être un peu bizarre. Je te fais le truc et j'en met un pour l'exemple. Voir catégorie:Liste d'innovations Anthere (d) 10 février 2013 à 19:43 (CET)
> 
> Bonsoir, Anthere !
> Pour les outils, il y un bouquin sur le sujet, celui de Géraldine Benoit-Cervantes, consultante à la CEGOS, La Boite à outils de l'Innovation. C'est la présentation de 66 outils. Cela peut faire l'objet d'une sous-catégorie : Les outils du management de l'innovation. À toi de voir.
> Pour la sous-catégorie liste d'innovation, merci ! C'est un sujet important et sensible. J'ouvre un paragraphe sur le sujet. --Jean-Louis Swiners (d) 11 février 2013 à 21:56 (CET)

---

> Une innovation, nouveau produit innovant, ressemble fort à un pleonasme tautologique qui voudrait dire la meme chose.--Invidi (discuter) 8 juin 2014 à 06:49 (CEST)

> Bonjour Invidi,
> Cela y ressemble fort mais ce n'est pas la même chose.
> Une innovation, c'est quelque chose, en général et le plus souvent un nouveau produit (et non pas un animal ou un sentiment ou que sais-je d'autre) qui, en plus d'être un nouveau produit, est innovant (c'est-à-dire qui apporte pour la première fois quelque chose qui n'avait jamais été apportée , alors qu'un nouveau produit peut très bien ne pas être innovant. Exemple : la Logan. Un autre exemple récent (2013) : la Vespa 946, nouveau produit peu innovant dont peu de gens penserons que c'est une innovation.
> Par contre, le Piaggio MP3, le scooter à 3 roues, dans cette optique, est une « vraie » innovation. Nouveau produit pour Piaggio, nouveauté au Salon de Milan 2006, innovant (pour la première fois un deux roues en a trois), implémenté mondialement, et avec succès (150.000 vendu en France depuis le lancement).
> Non, non, un nouveau produit ou une nouveauté innovante, ça fait bizarre mais ce n'est pas un pléonasme tautologique. il suffit de s'entendre sur ce qu'est un nouveau produit, une nouveauté et sur ce que veut dire innovant.
> Qu'en penses-tu ? Cordialement. --Jean-Louis Swiners (discuter) 8 juin 2014 à 09:09 (CEST)

> Bonjour, je persiste a penser que decrire une innovation par son caractere innovant est un pleonasme. J’aurais prefere : nouveau produit, service ou process presentant un caractere de rupture. Cordialement.--Invidi (discuter) 8 juin 2014 à 12:42 (CEST)

> Je suis d’accord avec Invidi (d · c · b), ce n’est peut-être pas un pléonasme mais pour un lecteur qui découvre le sujet, cela y ressemble furieusement.
> D’ailleurs Jean-Louis Swiners (d · c · b) je pense que tu viens toi-même de résoudre toi-même le problème, l’introduction pourrait partir de ta phrase : « Une innovation, c'est quelque chose, en général et le plus souvent un nouveau produit (et non pas un animal ou un sentiment ou que sais-je d'autre) qui, en plus d'être un nouveau produit, est innovant (c'est-à-dire qui apporte pour la première fois quelque chose qui n'avait jamais été apportée , alors qu'un nouveau produit peut très bien ne pas être innovant. » On pourrait simplifier en quelque chose comme « Une innovation est un produit qui apporte pour la première fois une fonctionnalité nouvelle qui n'avait jamais été apportée auparavant » (éventuellement en introduisant explicitement la notion de rupture d’Invidi ; et on peut même garder la phrase : « un nouveau produit peut très bien ne pas être innovant » pour le corps de l’article). Certes, on retrouver « innovation » et « nouveau » mais au moins il y a des qualificatifs pour expliquer et saisir la nuance.
> Cdlt, Vigneron * discut. 8 juin 2014 à 20:54 (CEST) 

---

> L'innovation vs le progrès
> Re VIGNERON (d · c · b) J'oubliais. Alors que le Progrès, comme l'Évolution pour Bergson, est exogène et sans finalité, l'innovation, elle, est, pour Drucker, et ça se défend, un progrès endogène ayant une finalité. C'est pour cela que j'en mentionne les objectifs. Ceci étant, on peut très bien ne pas être d'accord avec ceux-ci (ce sont ceux de l'OCDE). C'est un autre sujet ou une nouvelle section de l'article. Cdlt--Jean-Louis Swiners (discuter) 9 juin 2014 à 19:06 (CEST) 

---

> Je pense que le système en galerie est une très bonne idée à condition de bien l'étoffer.
> J'ai en outre une remarque quant à la pertinence de l'exemple de l'iPad mini : << Nov. 2012. Apple lance un nouvel iPad, l'iPad mini, nouveau produit qui ne comporte aucune innovation >>. Bien que minime, l'iPad mini est une innovation (passage d'une dimension d'écran du 9,7 pouces au 7,9 pouces). L'idée serait plutôt de nuancer le mot << Aucune innovation>>.
> --88.139.80.204 (discuter) 22 octobre 2014 à 01:47 (CEST)

Cette page doit être recyclée depuis 4 ans, l'innovation ça n'a pas l'air très innovant.

## Innovation has-been

innovation c'est ringard, old-school/old-fashioned, rétro, mort, cheesy, démodé, vieux-jeu

L'innovation n'en fini pas de mourir…

voir https://www.adamwitmer.com/blog/2013/5/12/redbox-innovation-has-been ?

innovation et être assis sur les épaules de géants (machiavel, le prince chap6 et eco, le nom de la rose)

## Innovation et excellence (Promising)

> [apprenez l'innovation autrement](https://www.promising.fr/promising/notre-histoire/)

> Formation des enseignants
> Pour que les enseignants portent à leur tour des initiatives pédagogiques innovantes, il est nécessaire de les former.

Dé-responsabilisation décrédibilisation du corps enseignant ?

S'il faut former les formateurs, qui forme les formateurs de formateurs ?

### Histoire

Excellence et innovation "initiatives d'excellence" (Idex).

Idex et promising ?

> 2011/2012
> Lancement de l'appel à projets "Initiative d’excellence en formation innovante" (IDEFI) dans le cadre des investissements d’avenir.
> L’Université Pierre Mendès-France est sélectionné pour son projet Promising ; seul IDEFI portant sur l'innovation et la pédagogie de l'innovation en sciences humaines et sociales à l'université.

https://www.promising.fr/promising/notre-histoire/

> Initiatives d’excellence (IDEX) fait partie des investissements d’avenir, programmes dont le but est de créer en France des ensembles pluridisciplinaires d'enseignement supérieur et de recherche qui soient de rang mondial.
> 
> Une vingtaine de sites font partie des IDEX, dont certaines en période probatoire ou interrompues.
> 
> Historique
> Article principal : Investissements d'avenir.
> PIA 1
> 
> Engagés par la loi de finances rectificative du 9 mars 2010, les investissements d’avenir (« PIA ») consacrent 7,7 G€ aux initiatives d’excellence, ce qui en fait de loin l’action la plus importante1. Les Initiatives d’excellence sont sélectionnées par un jury international après un appel à projets. Cette méthode avait déjà été utilisée pour le Plan campus en 2008.
> 
> Les dix-sept projets sont déposés en janvier 2011<sup>2</sup>. Les trois premiers lauréats sont connus en juillet : Bordeaux 1, Strasbourg et Paris Sciences Lettres3. En février 2012, François Fillon annonce les cinq initiatives retenues lors de la seconde vague : Sorbonne universités, Sorbonne Paris Cité, Saclay, Aix-Marseille et Toulouse. Le Premier ministre a également indiqué qu’un accompagnement des projets d'Hésam et de Lyon serait mis en place par le ministre de l’Enseignement supérieur et le Commissaire général à l'investissement (CGI)4. Les dotations sont annoncées quelques jours plus tard, et il apparaît que le total représente 6,35 G€ au lieu des 7,7 G€ initialement annoncés, ce qui s'explique par un redéploiement des fonds sur les Labex et les Idefi. De plus les dotations destinées aux Labex des initiatives d’excellence sont financées sur les dotations des Idex5. En avril 2016, à l’issue de la période probatoire et un examen par le même jury qu’en 2011, trois Idex ont été confirmés, trois (Saclay, Paris Sciences Lettre et Sorbonne universités) ont vu leur période probatoire renouvelée et deux ont été interrompues, (Toulouse et Sorbonne Paris Cité)6. En conséquence de l'interruption pour Toulouse et Sorbonne Paris Cité, les deux tiers de l'argent alloué est retiré7, et déposent à nouveau leur dossier un an après l'interruption8.

https://fr.wikipedia.org/wiki/Initiative_d%27excellence

### Université et excellence 
    
> Antiquité : Les centres d'enseignement réservés à l'élite

> L'article 13 du Pacte international relatif aux droits économiques, sociaux et culturels adopté à New York le 16 décembre 1966 par l'Assemblée générale des Nations unies stipule que : « L'enseignement supérieur doit être rendu accessible à tous en pleine égalité, en fonction des capacités de chacun, par tous les moyens appropriés et notamment par l'instauration progressive de la gratuité ».

https://fr.wikipedia.org/wiki/Universit%C3%A9#Les_classements_internationaux_des_universit%C3%A9s_et_leurs_probl%C3%A9matiques

Le seul financement pour la production (recherche), la conservation (publications et bibliothèques) et la transmission (études supérieures) est une injonction à l’excellence et l'innovation.

Excellence et imitation

---

# Notes Promising

Thierry Ménissier, auteur de, Machiavel ou la politique du centaure + Machiavel.le prince ou le nouvel art politique

_Conférence_ 13h45 24

Ethics _by_ design 

design éthique c'est le beau.

design éthique c'est émettre des _normativités_ ?

normativités : émettre jugement valeurs, standards ?

règlement prescriptions
loi
déontologie
morale
religion
éthique

Morale, Règles(lois, règlements et prescriptions, déontologie) + sens

faire le bilan de ses normativités contradictoires c'est être éthique ?

utilitarisme, calculer son intérêt, calculer l'action éthique

éthique = un calcul

Arétaïque : vertus (machiavel ?) et vices

Axiologique : une valeur supérieure

Déshumaniser l'éthique pour sauver la nature ? ( Pourquoi ne pas dé-rationaliser plutôt que de déshumaniser=rationaliser )

éthique en design = nouvelle idéologie politique (vraiment ? Une nouvelle forme du même discours plutôt ?)

_le progrès se détermine dans la confrontation dynamique des intérêts variés_

« innovation concept post progressiste qui ne se dit (pas), progrès débarrassé du scrupule humaniste. »

« Usagers au sens large, animaux, environnement. »

Design éthique c'est rendre vertueux quelque chose qui ne l'ai pas ?

« Les gadgets qui nous font plaisir on ne sait pas si c'est de l'innovation progressiste ou pas »

« Le marché fonctionne bien sans (vraiment?), l'éthique est drivée par l'usager »

« L'innovation c'est prendre des risques »

« le conflit des normativités »

AL = design est un ensemble de pratiques, d'individus, ce n'est pas éthique ou non

AL = Le design est une sorte d'attraction, une théorie qui peux se formuler en méthode, mais c'est avant tout des formes, des objets. fantasme d'un design qui résoudrait des problèmes

« design éthique revendication de la part des designers »

AL design fou, privé de parole puis sauveur (ordre du discours)

AL = en France particulièrement méprisé puis au début des années 2000 paré de toutes les vertus 

l'attention

---

# Sauver le monde et design soutenable

## Warka Water 

exemple design soutenable projet transdisciplinaire.

Soutenable > question de la ressource

distance point eau.

biomimétisme, traditions local

opposition homme nature ?

La nature comme designer ?

filet à brume (capte humidité comme toile)

la proposition un espace qui remplace(intègre ?) l'espace de réunion traditionnel ?

(aider les pays en voie de développement)

_sdf besoin de lois, protection/reconnaissance sociale, pas d'un manteau ?_

## Qualiforsol

projet d'architecture soutenable

Orléans (45)

construire en terre (architecture)

construire avec matériaux sur place

valoriser savoir faire (vivant, présent ?) = payer des travailleurs pas du matériel (savoir mort, passé ?)

murs très épais

__envisager la destruction__ (dust to dust) = vrai éthique ?

isolation (terre = mur en pierre), manteau d'isolation bois/paille

qu'est ce qui est bien de perdre 

---

# éthique en pratique

Graphiste design produit, enseignant

éthique plus ou moins applicable/appliqué

obsolescence programmée, comment lutter contre (ex machine à laver), monter entièrement l'objet, sans colle assemblage mécanique.

__démarche propre à la subjectivité du designer (vraiment ?). Lutter contre obsolescence ne répond à rien ? Sauf si le projet est vendu ?__

éthique et contrôle du projet (avoir la main sur le projet)

démarche inverse avec machine à laver samsung, projet initié par enjeux économiques, sociétaux, stratégique (objectif ?) <!-- projet initié ou non par le profit ou la domination d'un marché -->
designer chez samsung n'a pas la main sur les motivations du projet.

Éviter le projet ? Modifier le projet ? Es-ce-que c'est si grave d'être viré ou pas payé ? Choisir la difficulté ? (difficulté de celui qui offre/donne/sers ? = éthique ?)
Questionner la responsabilité individuelle 

Certains savoir-faire mènent à certains projets (non éthiques).
Questionner la responsabilité collective ? Passive ?

Position de créateur, l'auteur qui fait cahier des charges.

Mauvaises intentions existent pas ?

marge de manoeuvre, liberté personnelle, parole, point de vue possible = éthique

Prise de parole, point de vue.

---

# 250918, matin

## Préparation restitution groupe thématique "commun-quartier-mutation"

St-Bruno - politique locales : verticales / horizontales ?
Ateliers autour du magasin à St bruno 

budjet ville 

Fontbarlettes repas de quartier / inauguration

Gentrification ?!

Gène ? = initiatives qui tombent à côté -> rapport de force (sans eux) -> _production_ sans voix | don(unilatéral) dette, service -> évangélistes, colonisation
en Design - Caravane des droits -> lien appropriation
enjeux = faire disparaître ceux/acte de produire du lien -> laisser place

ONG -> Materiel mobilisé -> non utilisé (bloqué dans les aréoports), détourné

laisser la place au plus grand nombre ?

Grenoble -> population qui ne réclame pas ses droits | distance / mise à distance

~~

Piste = Théâtre ?
créer un cercle > avoir un bon leader/médiateur( sonne mieux ? Plus belle image ) | rassembler (dans le cercle / communauté) sans enfermer (dans le cercle)
production de symbole, mythe
_design_ =  créer des formes, des images ?
décor, surface, __IMAGE__

### _Comportement/enjeux_ au sein du groupe

produire des images(objets/dispositifs) (innovation?) [EF][pour penser et faire] , esquisser ?

Injonctions de la part du _facilitateur_ : Tout noter (quand bien même la taille du papier ou du stylo n'est pas du tout _choisie_), ce qui compte c'est _l'idée_ de mettre en commun et non la mise en commun _effective_ ?
Pas de téléphone ( :( google trad pour Vietnamien)

Eric Fache & Matali crasset ??
[EF][je suis grand spécialiste de l'interruption]

## Restitution groupe thématique "cultiver en ville"

végétation en ville = saleté (légumes,fruits de la ville serait selon le sens commun non comestible, voitures, pollution)
végétation en campagne = _propre_ (consommable, comestible)

usage des animaux en ville ?

Mairies cultive/plante du beau (fleurs), améliore esthétique à la limite lutte contre la pollution

cueillette - récupération

Comment on __fait(image)__ campagne (en ville)?
image -> produire du commun, image du commun?

-> critical design
!= comment on rend visible les interstices existants

transformer les pelouses en ronces
jardin fruitiers en ville

se donner le droit de cueillir

__Ville à croquer__ ville dégustable/comestible

cueillir mais en pensant aux autres (de manière vertueuse)

[EF+Designer Orga][théorie du carreau cassé (théorie qui s'avère apparement vrai à travers temps et expérience ?) : encourager comportements vertueux, si un carreau reste cassé d'autres casserons, si on le répare, d'autres seront réparer, DesignerOrga: métro de new york plein de tag, tags systématiquemnet effacés > gens arrêtent de tager…]

## Restitution groupe thématique "commun-quartier-mutation"

cercle mutant (mutations de quartier) | présence d'un médiateur dans le cercle plutôt qu'un leader

déclencher la parole (bottom - top, enpowererment) plutôt que donner unilatéralement/plaquer une structure _parfaite_ (top - down)

[EF][Faire figure] = point important ?

Le cercle et algorithmes de suggestion (youtube, spotify)
voir étude de google ?

-> réduit le cercle, mais c'est comme la vie (dans la vie on rétrécie, 80% fréquentations sont d'accord avec nous), si c'est comme la vie ça veux dire quoi ? Que c'est bien ?

## Petit interview/témoignage de Eric Fache sur question de innovation et image eric.fache@wanadoo.fr

### innovation et image ? (vous utiliser, pointer, encourager souvent l'image avec votre groupe, faire figure, théatre, forme, comment on fait campagne en ville…)

innovation -> convoquer un imaginaire
vertu projettante (faire projet) de l'image
-> celui qui a le verbe (l'image, le projet) à le pouvoir.
L'image est engageante, métaphore mène au partage, langage commun, l'image peux servir à cerner la pensée de l'autre

### Stratégie de l'image ?

Projection -> design fiction, futurs possibles, image provoque du débat

### Design sauve le monde ?

Mettre en critique cette formule.

En quoi augmente-on, maintient-on l'habitabilité/soutenabilité du monde (voir alain findeli)

Miguel Aubanis -> équilibre entre singularité et commun

Dérive mercantile du design, on re-dessine des cafetières

mais c'était quoi le projet du design

voir Bauhaus, Baudrillard aussi conserver le geste dans contexte industriel?, produire de meilleurs objets (rendre plus vertueuse l'industrie ?)

design éthique = renouveau contemporain de cette idée, dans l'air du temps, jeune génération : 
dans les années 80, création d'un bloc idéologie (global?) qui ostracise/austratise? tout le reste qui aujourd'hui s'effrite, laisse place aux jeunes -> désir de prendre place.

---

mail du duo pour question sur organisation : bra@ralstonbau.com

---

# 250918, après-midi

## groupe thématique créer du lien

moyen pour créer du lien, moyens (ex: stand de dégustation) réintégrés dans des scénario, histoires

scénario = fête de quartier, voisins, atelier cuisine -> journée de Tom 

Maïa : « j'ai l'impressin de jouer aux sims, malaise »

Designer (design de service) : la 27e région -> imaginer un service penser l'usager dedans ou imaginer une journée de quelqu'un et penser le service en fonction

- création de scénario, narrations, histoires, mythes
( design critique, fiction ? Création d'une pensée commune par la métaphore)

- à partir du scénario, on propose un objet, on _acte_ le scénario : restitution -> fresque 

certaines formules/images sont prononcées plusieurs fois (points de repères ?)

« partage et plaisir gustatif »
« partage/faire (l') interdit ensemble »

slogans ? modules ? pièces d'un système ? système de figures, d'images ?

[Bruno][c'est important de faire figure]

présentation représentation, comment transmettre les scénarios ?

Étudiante : « gobelet représente le contact avec la nourriture »

mise en scène de symbols, métaphores, création de commun ?

_data_-visualisation Time-line / volvelle (horloge)

Étudiante est perdue, pas l'occasion de s'expliquer sur son incompréhension, facilitateur coupe, lui _facilite_ la parole/ la compréhension en la désactivant/dévalorisant/faisant disparaitre ?

2h15 on(tout les groupes) prépare un ensemble de propositions (_force de proposition_) "graphiques" "visuelles" pour représenter solutions/ scénarios , produire du commun ?

[Bruno][design éthique, comment créer dialogue par exemple, proposition de prototypes éthiques, interet institutions, mairies ? Réponse à un problème, obtention d'un financement]

C'est le dispositif de l'horloge(fabriquée en carton, taille humaine) qui est choisi : l'heure tourne, les événements/éléments des trois scénarios apparaissent et disparaissent parallèlement/simultanément dans le temps


