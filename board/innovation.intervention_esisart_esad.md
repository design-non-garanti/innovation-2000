<meta charset="utf-8">

# Innovation

## Innovation comme outil rhétorique, innovation comme acceptation sociale (emile)

- Appel à projets IDEX Rayonnement Social et Culturel 2018
- IDEX (Initiative d'Excellence),

> Dans le cadre de la labellisation IDEX  la Communauté d'Universités et
> d'Etablissements Université Grenoble Alpes (ComUE) lance son appel à projets
> Rayonnement Social et Culturel 2018 pour soutenir des initiatives en culture
> et culture scientifique en lien avec l'université.

> L'appel à projet s'inscrit dans les objectifs transversaux de l'Idex :
> excellence, innovation, attractivité, rayonnement et développement de
> partenariats. 

- à la lecture des documents, appels, sites, etc
- se dégage un aspect à première vue banal qui reste à questionner : la notion d'innovation

- mot très actuel, dont en entend beaucoup parler (Macron)
- qualifié de buzzword, de mot à la mode
- finalement on ne sait pas trop ce que ça veut dire
- on se doute de sa proximité avec les concepts d'« invention » et de « nouveauté »
- il y a différents mots = il existe probablement des divergences, des subtilités

- innovation a plusieurs sens 
- pas un seul sens défini 
- le sens de ce concept a évolué au fil du temps

- l'etymologie d'« innovation » et d' « innover »
- du moyen français, latin, italien
- rapelle la relation avec le « renouvellement » et l'« action d'introduire une chose nouvelle » 

> (XVIIe siècle) Du moyen français innovation (« action d'introduire une chose
> nouvelle »), lui-même de l'ancien français innovacion emprunté au latin
> innovatio (« **renouvellement** »)[1]. Il est apparu à côté de novation, qui est
> resté cantonné au domaine du droit.  (1957) Emprunt à l'américain innovation,
> s'agissant du domaine économique et de la gestion d'entreprises[2]. 

Innovation, https://fr.wiktionary.org/wiki/innovation

> (1315)[1] Du latin innovare (« renouveler »)[2], de novare (« renouveler » ; «
> inventer » ; « innover »)[3].  (ca. 1960) Calque de l'anglais américain innovate
> et innovation (« changer une organisation en profondeur / changement la façon de
> fonctionner pour une organisation, une entreprise... »)[4].

Innover, https://fr.wiktionary.org/wiki/innover

- l'article Wikipedia portant sur l'innovation propose une définition

> Concrètement, une innovation est quelque chose qui, produit ou reproduit en grand
> nombre et commercialisé ou déployé pour la première fois avec succès, a
> amélioré, changé, modifié, transformé ou révolutionné un secteur d'activité, une
> pratique sociale ou la vie d'un grand nombre d'individus, ceci le plus souvent
> de façon inattendue et inconsciente.

Innovation, https://fr.wikipedia.org/wiki/Innovation

- le site de la cité des sciences propose une définition

> Innover, c'est réussir le pari de lancer de nouveaux produits [...]
> intégrer le meilleur des connaissances dans un produit ou service créatif qui
> permet d'aller plus loin dans la satisfaction des individus.

L'innovation qu'est ce que c'est ?, http://www.cite-sciences.fr/archives/francais/ala_cite/expositions/observatoire-innovations/definition-innovation/definition-innovation-1.html

- « produit ou reproduit en grand nombre » -> renouvellement
- « commercialisé ou déployé pour la première fois avec succès » -> réussir à introduire

- « réussir le pari de lancer de nouveaux produits » -> réussir à introduire
- « aller plus loin dans la satisfaction des individus » -> réussir à introduire

- au regard de ces définitions on peut se demander
- l'innovation, innover ou un produit innovant
- est en fait un produit qui marche, qui s'est bien vendu, qui est accepté
- l'innovation consisterait-elle à « réussir à lancer » des produits
- non pas à créer des choses qui n'existaient pas auparavant
- un lien étroit avec le business, marketing, marché, réussite, vente, capital

- Anthony Masure s'appuyant sur Walter Benjamin
- propose un regard sur l'« invention » et l'« innovation »

> L'invention désigne la création par un ou plusieurs individus d'une
> technique ou d'un procédé technique qui n'existait pas auparavant; ainsi de la
> photographie ou d'Internet. L'invention est rarement financée par du
> capital (de l'argent investi) car elle est le fait d'un petit nombre
> de personnes qui ne savent pas ce qu'il y a à faire avec. Son
> caractère inattendu s'oppose à l'investissement. L'invention, en tant qu'idée
> singulière, n'est pas pensée dans le cadre d'un marché économique ou d'un
> progrès. 

Anthony Masure, Le design des programmes, 2014, p. 124. 

- « L'invention désigne la création par un ou plusieurs individus d'une
  technique ou d'un procédé technique qui n'existait pas auparavant; »
- on ne sait pas « ce qu'il y a à faire avec » -> difficile "d'investir"
- « caractère inattendu » -> difficile "d'investir"
- «  L'invention, en tant qu'idée singulière, n'est pas pensée dans le cadre
  d'un marché économique ou d'un progrès. » 

> Prise dans la double direction contradictoire du progrès benjaminien, la
> conception habituelle de l'innovation repose sur l'acceptation sociale de
> l'invention. L'économie peut ainsi inscrire l'invention dans des formes
> anachroniques, comme l'exemple des cartes de visite photographiques. Si l'on
> suit Walter Benjamin, l'innovation est donc tout à fait autre chose que la
> découverte d'une technique. Alors que l'innovation a pour but premier
> d'installer et de pérenniser une position sur un marché, la découverte se
> rattache à la compréhension en acte des spécificités d'une technique.
> L'innovateur (synonyme d'entrepreneur dans ce contexte) est celui qui parvient à
> trouver une application capitalisable à l'invention. C'est donc la conquête
> d'un marché précis qui va définir l'innovation. Cette capitalisation d'un
> secteur repéré a pour conséquence de retirer du domaine public ce qui était
> auparavant disponible gratuitement ou à moindre coût. Il en est ainsi, par
> exemple, des sociétés dites « de services », qui investissent progressivement
> toutesles bribes de l'espace social.

Anthony Masure, Le design des programmes, 2015, p. 125.

- « innovation repose sur l'acceptation sociale de l'invention » -> réussir à introduire
- « l'innovation a pour but premier d'installer et de pérenniser une position
  sur un marché » -> réussir à introduire
- « L'innovateur (synonyme d'entrepreneur dans ce contexte) est celui qui
  parvient à trouver une application capitalisable à l'invention. C'est donc la
  conquête d'un marché précis qui va définir l'innovation. » -> réussir à
  introduire
- «  Cette capitalisation d'un secteur repéré a pour conséquence de retirer du
  domaine public ce qui était auparavant disponible gratuitement ou à moindre
  coût. » -> fermeture

- retour sur le site de la cité des sciences
- on percoit aussi une distinction entre invention et innovation

> L'innovation se distingue de l'invention ou de la découverte en ce sens
> qu'elle suppose un processus de mise en pratique, aboutissant à une
> utilisation effective.

L'innovation qu'est ce que c'est ?, http://www.cite-sciences.fr/archives/francais/ala_cite/expositions/observatoire-innovations/definition-innovation/definition-innovation-1.html

- processus de mise en pratique -> réussir à introduire

ce serpent se mord t-il la queue ?
  * se dit innovant ce qui a été accepter socialement (introduit)
  * ce qui a été accepter socialement (introduit) se dit innovant

- le terme innovation
- qui n'est pas anodin, neutre
- rassure, indique le "progrès", le changement
- permet d'asseoir un discours, de gagner une confiance, de passer pour un professionel
- outil rhétorique

- Machiavel dans Le prince

> Une histoire de l'idée d'innovation montre comment le sens de ce concept a
> évolué depuis les Grecs, Xénophon et Aristote. Elle est ensuite réintégré,
> dans un sens politique par Machiavel dans Le prince en 1513, puis par F. Bacon
> en 1625. Pour Machiavel, il s'agit de donner une impression de renouvellement
> d'un système politique afin d'en garder le pouvoir en période de trouble.

Innovation, https://fr.wikipedia.org/wiki/Innovation

- Benoit Godin dans son papier sur Machiavel (Le prince + The discourses)

> This paper looks at what innovation is to Machiavelli. Using both The
> Prince and The Discourses, the paper documents the central role innovation
> occupies in politics. To Machiavelli, innovation is a means to
> stabilize a turbulent world rather than revolutionize it in contrast
> to what theorists of innovation say today.

Benoit Godin, The Politics of Innovation : Machiavelli and Political Innovation, or, How to Stabilize a Changing World

(il faudrait peut-être acheter le bouquin de Godin sur l'histoire de l'innovation...)

- outil politique et rhétorique
- opposer au sens actuel


- Don Norman, *The Invisible Computer : Why Good Products Can Fail, The Personal Computer Is So Complex, and Information Appliances Are the Solution*
- pourquoi les bons produits peuvent échouer ? car mauvais business
- dès la préface Norman évoque le cas d'Edison et de l'invention du phonographe

> Thomas Edison was a great inventor but a poor businessman.

Donald Norman, *The Invisible Computer*, 1999, p. ?

- Pour Norman le plus important est le marketting non pas les qualités d'un objet
- un objet trop complexe est difficile à marketter, faire accepter, etc. (échec du phonographe car trop complexe)

- innovation/innovant est aujourd'hui synonyme de "bon"
- mais la mesure de la qualité se fait par sa réussite sur le marché et non sur
  les caractéristiques intrinsèques d'un objet
- Est-ce qu'un objet fameux et très utilisé (bien introduit donc) implique de bonne caractéristiques ? ...

- autre aspect :
- innovation pour résoudre tout les problèmes du monde (comme le design
  d'ailleurs)
- innover pour éviter de se concentrer sur des changements de fond

Nous arrivons ainsi à l'hypothèse d'une possible "propagande de l'innovation",
idéologie visant à résoudre tout besoin, problème ou désir par la production
d'artefacts et de concepts en constante évolution, justifiant l'obsolescence
technologique au nom de la vitalité économique à court terme. 

> the consumption / innovation duality as the driving force behind our economy.

Statement, disnovation.org

> The notion of innovation is the ultimate contemporary rhetorical tool 

Statement, disnovation.org

> La ruée continue vers la nouveauté et la négation des valeurs précédentes est-elle une nécessité humaine, une tendance intuitive, une fin en soi ?
> L'innovation est-elle l'expression d'un idéal particulier dont les finalités sont dictées par de simples choix économiques et industriels ?
> Comment les artistes deviennent-ils des agents tacites dans la diffusion et la vulgarisation des innovations ?
> Quelles pratiques critiques, subversives ou alternatives cette situation engendre-t-elle en retour ?

Statement, disnovation.org

- innovation et obsolescence / innovation et fermeture

1. proposer un objet innovant
2. celui ci s'essoufle car il n'est pas "ouvert" (Simondon / Huygues).  dès sa
   sortie il se dégrade petit à petit car l'utilisateur ne peut pas l'entetenir
   / le réparer / le modifier / l'adapter / l'améliorer.
3. go to 1.

## Maxime

## innovation, entre la nouveauté et sa négation.

- avant-garde principe de nouveauté, d'invention (par excellence ?)

- tandis que l'avant-garde (dans une posture auto-référente) s'attache à détruire l'existant, à en faire table rase (dans un soucis de nouveauté ?)
- détruire l'ordre pour en imposer un autre, un nouveau (création de la modernité)

- le processus innovateur lui se réfère au pré-existant et s'attache à le réactualiser
- reproduire l'ordre pour l'imposer à nouveau
- système de mode
- il se rapproche du kitsch (Moles?) dans la production d'inauthentique, dans cet attitude reproductrice et multiplicatrice, sa relation avec le domaine industriel et la consommation de masse (reproduction de la modernité)

L'innovation à mi-chemin entre kitsch et avant-garde ?

> La catégorie introduit tout d'abord une comparaison entre deux formes de vie : une forme fermée qui produit le sens par répétition des formes ; une forme ouverte qui proscrit au contraire la répétition et revendique la nouveauté. Elle met ainsi en évidence deux modes de fonctionnement des stratégies culturelles, où l'un permet de prévoir les formes qui se stabilisent en système, tandis que l'autre rejetant tout système, n'autorise aucune prévision.
> -- <cite>Anne Beyaert-Geslin « Kitsch et avant-garde », Actes Sémiotiques. 2007.</cite>[<sup>9</sup>][9]

### innovation et obsolescence 

l'innovation affirme et nie simultanément l'existant, il s'agit de progresser à sens contraire, (chercher à, ) être en tête le regard tourné vers le passé, l'établi.

> Ils ne veulent pas le progrès, Ils veulent être les premiers
>
> -- <cite>Six fois deux - 3a (Jean-Luc Godard, 1976)</cite>[<sup>7</sup>][7]

Il n'est pas rare que l'innovant s'inspire du « retro », qu'il tende vers le recyclage en remettant au goût du jour des pratiques artisanales, locales.

ex:

(Paradoxalement) il pourrait s'agir de re-questionner nos acquis, de faire le procès de nos déterminations. (pas seulement en apparence)

> Introduction : exploration pavillonaire¹
> Fausse cheminée d'angle, en bois mouluré et façonné comme du marbre. Elle dissimule en fait un petit placard, nous sommes dans un pavillon fort modeste des années cinquante et la cheminée véritablement fonctionnelle n'est plus à l'ordre du jour. Elle réapparaîtra un peu plus tard associant fonctionnalité et valeurs de tradition, d'authenticité dans nécessairement être qualifiée de « kitsch ».
> […]
> « ¹. Se dit d'un style ou d'une attitude esthétique caractérisés par l'usage dévié d'éléments démodés ou populaires produits par l'économie industrielle, considérés comme de mauvais goût par la culture établie et valorisés dans leur utilisation seconde. […] »
> -- <cite>Odile le Guern « Kitsch, ça le fait ! », Actes Sémiotiques [En ligne]. 2007.</cite>[<sup>8</sup>][8]

Le paradoxe de l'innovation est celui de reproduire « l'ordre du jour » au travers d'objets en contradictions avec celui-ci

Pour nous, son intérêt réside dans la production ou la mise en avant d'objets en 

machines matériel techniques logiciels obsolètes désuet
bas sale simple rapide bon marché pratique populaire recyclage réparable local artisanal prosommation (production par l'utilisateur)

Le retrocomputing, ou rétro-informatique, est une activité consistant à utiliser du matériel et des logiciels informatiques obsolètes[<sup>1</sup>][1]

Le low-tech ou basse technologie, par opposition à high-tech, est un ensemble de techniques apparemment simples, pratique, économiques et populaires. Elles peuvent faire appel au recyclage de machines récemment tombées en désuétude. Ce sont des solutions techniques qui cherchent à être simples, bien pensées, bien dimensionnées et réparables et une fabrication plus locale, plus proche de l'artisanat que de la production industrielle. Il peut aussi s'agir de prosommation, c'est à dire production par l'utilisateur. Il n'existe toutefois pas de réelle définition du Low-tech.[<sup>2</sup>][2]

Lo-fi (abr. de low-fidelity, « de basse fidélité ») est une expression apparue à la fin des années 19801 aux États-Unis pour désigner certains groupes ou musiciens underground adoptant des méthodes d'enregistrement primitives pour produire un son « sale », volontairement opposé aux sonorités jugées aseptisées de certaines musiques populaires2. L'expression est l'antonyme de hi-fi (abr. de high-fidelity, « de haute fidélité »3).[<sup>3</sup>][3]

Dans un contexte informatique où le calcul de très nombreux polygones est chose banal, où l'imagerie numérique commune tend vers l'hyperréalisme – est appelé "low-poly" une représentation informatique géométrique simplifié d'un objet en trois dimensions. Une technique donc esthétique plutôt qu'une technique d'optimisation. Ils sont souvent utilisés par les développeurs indépendants, plus rapides et moins chers à créer.
Les graphismes "low-poly" peuvent être utilisés pour obtenir un certain style rétro similaire au pixel art des jeux vidéo 'classiques'.[<sup>4</sup>][4]

Ex : Indie Game: The Movie réalisé par James Swirsky et Lisanne Pajot (2012) film documentaire qui suit Edmund McMillen et Tommy Refenes pendant le développement et la sortie de Super Meat Boy, Phil Fish travaillant sur Fez, ainsi que Jonathan Blow à propos de Braid[<sup>5</sup>][5]

L'expression jeu vidéo indépendant, comme dans le secteur de la musique ou du cinéma, désigne des jeux vidéo créés sans l'aide financière d'un éditeur de jeux vidéo. Le jeu vidéo indépendant connait une montée en puissance depuis le début des années 2000, notamment grâce à la distribution dématérialisée des jeux vidéo.
Deux exemples notables sont World of Goo1, sorti en octobre 2008, ainsi que Minecraft en mai 2009.[<sup>6</sup>][6]

[1]:https://fr.wikipedia.org/wiki/Retrocomputing
[2]:https://fr.wikipedia.org/wiki/Low-tech
[3]:https://fr.wikipedia.org/wiki/Lo-fi#Articles_connexes
[4]:https://en.wikipedia.org/wiki/Low_poly
[5]:https://fr.wikipedia.org/wiki/Indie_Game:_The_Movie
[6]:https://fr.wikipedia.org/wiki/Jeu_vid%C3%A9o_ind%C3%A9pendant
[7]:https://www.youtube.com/watch?v=AghSqlf9pyY
[8]:http://epublications.unilim.fr/revues/as/3292
[9]:http://epublications.unilim.fr/revues/as/3292

### Nous proposons un projet d'innovation low-tech

Le but n'étant pas de rendre innovant la low-tech mais de s'en servir pour détourner/approfondir cette notion d'innovation






## Notes de Maxime


innovation nouveauté avant-garde (kitsch?)
grenoble et innovation

byte beat & innovation ?

Art numérique et innovation ?

Art et science = innovation ? Pourquoi ?

réalisation de démonstration & démonstrateurs (d'un prototype)

projet et innovation

rapport au temps économique

culture et culture scientifique

---

innovation = faire plus vite, plus fort, meilleur, plus excellent

---

# Présentation

nous :

## Définir un axe de recherche collectif

Un projet de recherche collectif
comme une expo un coté curatorial cohérent
Encourage le croisement des ressources / sources entre les groupes.

Un axe, une idée qui met en relief le projet idex.

Quels en sont les limites, les objectifs, le discours.

Comment profiter de ce contexte pour réfléchir au rôle de l'art numérique.
À la relation entre l'idée d'innovation et d'art numérique, l'art numérique à t'il du sens hors de l'innovation.

---

transmedial
ars eletronica

---

la rétro-informatique (Retrocomputing) comme projet d'innovation

Inauthenticités de l'innovation (Anthony Masure)

innovation

(XVIIe siècle) Du moyen français innovation (« action d'introduire une chose nouvelle »), lui-même de l'ancien français innovacion emprunté au latin innovatio (« renouvellement »)[1]. Il est apparu à côté de novation, qui est resté cantonné au domaine du droit.
(1957) Emprunt à l'américain innovation, s'agissant du domaine économique et de la gestion d'entreprises[2].

innovo

De novō avec le préfixe in-.
Renouveler

novo

De novus (« neuf »).
Renouveler, rafraichir, changer.

retro (Anglais)

Adjectif

Du français rétro.
Rétro, démodé.
Retro style is the style of the 50s, 60s or 70s. It is currently undergoing a revival with many new products coming out with a retro look.

français

(Adjectif 1) (1974) Apocope de rétrospectif.
(Adjectif 2) (1889) Apocope de rétrograde.
(Nom) (1935) Apocope de rétroviseur.

Latin

De re-, → voir citro, ultro et intro.

Adverbe 

Derrière, par derrière, en arrière.
Vade retro Satanas.
À reculons, en remontant dans le passé.
En sens contraire, à rebours.

http://innovation.cma-languedocroussillon.fr/innovation-et-artisanat
