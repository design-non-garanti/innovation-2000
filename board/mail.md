Salut Dominique, 

Toutes nos félicitations ! :)

Nous nous sommes dit qu'il était nécessaire de te faire un récapitulatif sur la réunion de mercredi ainsi qu'un retour sur nos impression.

Petit récap de la réunion :

il y avait :
-  3 étudiants conservatoire (2 "musique à l'image" + 1 jazz) + Jean-Paul
-  pas d'étudiants ESISAR malheureusement + Vincent + Jérome
-  8-9 DG2 
-  Annick + Gilles
-  nous 2

en gros :
- on a refait un tour des acteurs du projet pour les personnes qui n'étaient pas
  présent à la première réunion.
- Vincent nous explique qu'il faut faire des fiches afin de décrire les projets.
  Ces derniers doivent être innovants et vendeurs afin d'attirer les étudiants
  ESISAR, être sélectionnés et réalisés.
- Les DG2 ont désapprouvé cette séparation entre ingénieurs et designers
- Jérôme a fait médiateur entre le vocabulaire entrepreneuriale de Vincent et
  Annick et Gilles.
- De notre côté c'était gênant, on se demandait : mais qu'est ce qu'on est
  en train de proposer aux DG2 ?!
- Gilles propose d'envisager un ou plusieurs workshops à Valence avec des étudiants
  ESISAR et les DG2 futur DG3 avec un (très) ancien étudiant de l'ESAD (on ne se
  souvient plus de son nom).
- Les étudiants de "musique à l'image" semblaient intéressés par le projet, posant la question d'un même projet étalé sur plusieurs années dans sa réalisation.
- Différents points de la réunion ont portés sur l'organisation à long terme de cet échange
- De notre côté nous avons présenter rapidement la pratique du bytebeat et évoquer nos petites expérimentations puis nous avons introduit nos recherches sur la notion d'innovation, terme revenant constamment dans le projet et particulièrement dans cette réunion. On a tenté d'amener une approche critique du terme (les notes
  sont là : http://0000000.chickenkiller.com/innovation-2000/innovation/innovation.md). On a parler de l'innovation comme acceptation sociale d'un produit / passage nécessaire à son introduction sur un marché. La problématique de l'innovation comme un terme amalgamant succès commercial et avant-garde. De la relation entre innovation et
  obsolescence. À partir de ce discours nous avons proposer plusieurs pistes de projets : 
  faire des objets non-innovants / disnovants http://disnovation.org/,
  “un projet d'innovation low-tech” non dans l'optique de rendre innovant la low-tech mais de s'en servir pour questionner l'innovation et ses paradoxes - innovation qui nie
  l'existant tout en le réactualisant sans cesse.
  En conclusion, nous avons proposer pour blaguer une réalisation innovante absurde : le bytebeatcoin.
  La musique ne générant pas assez d'argent. Faisons en sorte que l'argent
  génère de la musique. (Voir rémunère ses auditeurs).
  Le bytebeat repose sur une boucle itérative et miner de la cryptomonnaie
  consiste à résoudre des hashs un par un. On peut utiliser le total de hashs
  résolu comme variable itérative du bytebeat.
 
  Nous avons eu le sentiment que cette présentation à eu un certain effet, Vincent à trouvé vendeur l'idée du bytebeatcoin, Jérôme semblait motivé par notre intervention, Annick à eu l'air d’apprécier notre critique.

  Cette idée du bytebeatcoin était plus pour nous une façon de tenter donner un ton au projet, d'exprimer notre attitude.

  Vincent nous a appris que la cryptographie hardware était une pratique de ses élèves, la cryptographie ou la stéganographie pourrait être une thématique de projet.

  Nous aimerions parler de tout ça avec toi rapidement il semble que le temps soit une contrainte majeure : le dépôt des "fiches projets" doit se faire en juin. Une contrainte qui dissocie clairement, "conception" (étudiants ESAD) et "réalisation" (étudiants
  ESISAR). C'est pas tip top… De plus il y a encore de nombreuses zones d'ombres dans ce projet, de quelles contraintes, acteurs dépendons-nous vraiment ? De quel manière les projets vont-ils être financer par rapport aux documents que tu nous as envoyé. 
  Pouvons-nous instaurer une situation de travail collective réelle, ou s'agira t-il d'une sélection arbitraire de projets afin de cocher la case art numérique de l'idex ?

  Nous aimerions aussi pouvoir nous retrouver à nouveau tous ensemble, Annick Gilles toi et les DG2 pour discuter de tout ça.

  En tout cas profite bien de tes marmots,

  à très bientôt !












- Il y a beaucoup d'éléments pas super clairs pour nous, notamment sur l'organisation et le
  déroulement du projet, dont nous aimerions prendre connaissance assez
  rapidement :














- bonjour
- bravo bébé
- 2 parties :

0) récap réunion

- je te laisse faire

1) futur, suite, proposition

- Discuter avec Jérôme et Vincent pour qu'il nous explique clairement le
  déroulement, la structure du projet (acteurs, financements, déroulement,
  rendu, publication, etc.). Ces éléments ne sont pas clairement dits selon moi
  (c'est presque génant). Structure qu'il faudra probablement détourner car
  elle semble difficilement permettre d'instaurer une situation de travail mais
  plutôt de cocher une case





























Bonjour Annick, Gilles, Dominique,

Suite au rendez-vous du 2 mai nous vous envoyons un mail "à chaud" pour
nous faire part de nos interrogations.

- Il y a beaucoup d'éléments pas super clairs pour nous, notamment sur l'organisation et le
  déroulement du projet, dont nous aimerions prendre connaissance assez
  rapidement :
  * Nous sommes censés proposer des "fiches projets" afin qu'elles soient ou non
    acceptées / retenues. 
    - Combien d'entre elles seront sélectionnées ?
    - Par qui (les étudiants ESISAR,  les profs, des entreprises, etc.) ?
    - Sommes nous les seuls à proposer ces fiches projets ?
    - Il y a t'il d'autres projets porté par des autres acteurs que nous n'avons
      pas rencontré dans le même contexte

  * Les étudiants ESISAR existent-ils vraiment ?
    - si oui ils ont probablement des opinions

"les projets innovations" s'inscrivent dans un cadre plus large



- Il semble que le temps soit une contrainte majeure : le dépot des "fiches
  projets" doit se faire en juin. Contrainte qui serait responsable de la
  relation que les acteurs du projet doivent adoptés : dissocier, comme
  cela a pu être évoqué, "conception" (étudiants ESAD) et "réalisation" (étudiants
  ESISAR).
  
  les
  projets des étudiants . 
  
  Cependant la majeure partie du travail se
  déroulera d'octobre à décembre environ
  les projets sont ils acceptés par les étudiants, par des professeurs, des
  entreprises, etc.


- il semble 

- combien 

il nous semble important d'éclaircir ces points afin d'envisager la suite du
projet.


interrogations :

- beaucoup d'inconnues 
- on a pas vu les étudiants ESISAR, quelles sont leurs envies ? Comment les impliqués ? Pourrait-ils nous proposer des projets
- en savoir plus quand à la structure
- déroulement du projet
- seulement 4 projets art numérique, quels sont les autres projets, sont t-ils exposés avec les projets art numérique, si on travaille avec des entreprises

bourse idex que pour l'art numérique

partager notre dossier git

retrouver avec les dg2 intéressés pour formuler une proposition de projet commune ou quatre projets ou un axe de recherche commun, avec les professeurs

- 4 papiers à rendre ?
- 1 papier fait par 3 groupes
- 2 papiers fait par un groupe ?
