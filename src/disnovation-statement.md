# Statement

Over the past few decades, industrialised societies have experienced an unprecedented technological boom. The advent of information and communication technologies, irrigating whole domains of our existence, has deeply transformed our relationship with the surrounding world. This global phenomenon has contributed to put techno-sciences at the core of our belief systems and the consumption / innovation duality as the driving force behind our economy.

The notion of innovation is the ultimate contemporary rhetorical tool, spreading from the technoscientific field into the sectors of politics, management, eduction and art.
Thus we arrive at the hypothesis of a possible "propaganda of innovation", as an ideology aiming to solve any need, problem, or desire through the production of constantly changing artifacts and concepts, justifying technological obsolescence in the name of short-term economic vitality. This simple hypothesis raises many questions:
- Is the continuous rush towards novelty and the negation of preceding values a human necessity, an intuitive tendency, an end in itself?
- Is innovation the expression of a particular ideal whose purposes are dictated by mere economic and industrial choices?
- How do artists become tacit agents in the spreading and popularisation of innovations?
- What kind of critical, subversive or alternative practices does this situation generate in return?

Nicolas Maigret, Bertrand Grimault [2012-2014]

DISNOVATION is an open event architecture that spreads across a wide range of strategies: lectures, debates, exhibitions, screenings, performances, workshops and documentation. 
