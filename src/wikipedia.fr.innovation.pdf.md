Si ce bandeau n'est plus pertinent, retirez-le. Cliquez pour voir d'autres modèles. Cet article contient un travail inédit ou des déclarations non vérifiées (février 2013).
Vous pouvez aider en ajoutant des références ou en supprimant le contenu inédit. Voir la page de discussion pour plus de détails.

Si ce bandeau n'est plus pertinent, retirez-le. Cliquez pour voir d'autres modèles. Cet article n’est pas rédigé dans un style encyclopédique (février 2013).
Vous pouvez améliorer sa rédaction !